package com.supplychainmanagement.dto;

import com.supplychainmanagement.model.Requisition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemDto implements Serializable {

    long id;

    String name ;

    long price;

    int quantity;

    int quantity_per_carton;

    private List<ImageDto> imageList;

//    RequisitionDto requisition ;

    private long product_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity_per_carton() {
        return quantity_per_carton;
    }

    public void setQuantity_per_carton(int quantity_per_carton) {
        this.quantity_per_carton = quantity_per_carton;
    }

    public List<ImageDto> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageDto> imageList) {
        this.imageList = imageList;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public List<String> getImagePaths() {
        if (imageList == null || id == 0) {
            return null;
        } else {
            List<String> imagePaths = new ArrayList<>();
            for (ImageDto imageDto : imageList) {
                String imageName = "/uploaded-image/product/" + id + "/" + imageDto.getName();
                imagePaths.add(imageName);

            }
            return imagePaths;
            //return "/uploaded-image/product/" + id + "/" + image;

        }
    }
}
