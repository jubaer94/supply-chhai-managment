package com.supplychainmanagement.dto;

import com.supplychainmanagement.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto implements GrantedAuthority ,Serializable{

    private  long id ;
    private String authority;
    private List<UserDto> users ;
    @Override
    public String getAuthority() {
        return this.authority;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "id=" + id +
                ", authority='" + authority + '\'' +
                '}';
    }
}
