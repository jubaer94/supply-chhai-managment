package com.supplychainmanagement.dto;

import com.supplychainmanagement.model.Image;
import com.supplychainmanagement.model.Payment;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.model.Role;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements UserDetails , Serializable {

    private long id;
    private String username;
    private String password;
    private String email;
    private List<RoleDto> roles;
    private List<RequisitionDto> requisitions;
    private boolean enabled ;
    private long paymentDue ;
    private boolean paymentClearStatus;
    private List<PaymentDto> payments;
    private List<ImageDto> imageList;
    private MultipartFile[] multipartFiles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<RoleDto> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDto> roles) {
        this.roles = roles;
    }

    public List<RequisitionDto> getRequisitions() {
        return requisitions;
    }

    public void setRequisitions(List<RequisitionDto> requisitions) {
        this.requisitions = requisitions;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(long paymentDue) {
        this.paymentDue = paymentDue;
    }

    public boolean isPaymentClearStatus() {
        return paymentClearStatus;
    }

    public void setPaymentClearStatus(boolean paymentClearStatus) {
        this.paymentClearStatus = paymentClearStatus;
    }

    public List<PaymentDto> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentDto> payments) {
        this.payments = payments;
    }

    public List<ImageDto> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageDto> imageList) {
        this.imageList = imageList;
    }

    public MultipartFile[] getMultipartFiles() {
        return multipartFiles;
    }

    public void setMultipartFiles(MultipartFile[] multipartFiles) {
        this.multipartFiles = multipartFiles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }


    public List<String> getImagePaths() {
        if (imageList == null || id == 0) {
            return null;
        } else {
            List<String> imagePaths = new ArrayList<>();
            for (ImageDto imageDto : imageList) {
                String imageName = "/uploaded-image/user/" + id + "/" + imageDto.getName();
                imagePaths.add(imageName);

            }
            return imagePaths;
            //return "/uploaded-image/product/" + id + "/" + image;

        }
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
