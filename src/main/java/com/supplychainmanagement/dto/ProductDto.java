package com.supplychainmanagement.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.supplychainmanagement.model.Category;
import com.supplychainmanagement.model.Image;
import com.supplychainmanagement.model.Requisition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProductDto implements Serializable {


    long id;
    String name ;
    long price;
    int quantity_per_carton;
    int inStock;
    @JsonIgnore
    private CategoryDto category;
    @JsonIgnore
    private List<RequisitionDto> requisitions ;
    private List<ImageDto> imageList;
    private MultipartFile[] multipartFiles;
    private boolean enabled = true;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getQuantity_per_carton() {
        return quantity_per_carton;
    }

    public void setQuantity_per_carton(int quantity_per_carton) {
        this.quantity_per_carton = quantity_per_carton;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public List<RequisitionDto> getRequisitions() {
        return requisitions;
    }

    public void setRequisitions(List<RequisitionDto> requisitions) {
        this.requisitions = requisitions;
    }

    public List<ImageDto> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageDto> imageList) {
        this.imageList = imageList;
    }

    public MultipartFile[] getMultipartFiles() {
        return multipartFiles;
    }

    public void setMultipartFiles(MultipartFile[] multipartFiles) {
        this.multipartFiles = multipartFiles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getImagePaths() {
        if (imageList == null || id == 0) {
            return null;
        } else {
            List<String> imagePaths = new ArrayList<>();
            for (ImageDto imageDto : imageList) {
                String imageName = "/uploaded-image/product/" + id + "/" + imageDto.getName();
                imagePaths.add(imageName);

            }
            return imagePaths;
            //return "/uploaded-image/product/" + id + "/" + image;

        }
    }
}
