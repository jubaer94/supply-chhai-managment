package com.supplychainmanagement.dto;

import com.supplychainmanagement.model.Payment;
import com.supplychainmanagement.model.Product;
import com.supplychainmanagement.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequisitionDto implements Serializable {

    Random random = new Random();
    private long id ;
    private UserDto user;
    private String referenceId;
    private List<PaymentDto> payments;
    private long paymentDue;
    private List<ItemDto> items;
    private LocalDateTime createdAt;

    private long total;
    private boolean sentForPaymentVerification = false ;
    private boolean firstPaymentApproved = false ;
    private boolean approvedByInventoryManager = false;
    private boolean paymentClear = false;
    private boolean receivedByUser = false;
    private boolean enabled = true;
    private Boolean approvedByDeliveryIncharge = false;

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public List<PaymentDto> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentDto> payments) {
        this.payments = payments;
    }

    public long getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(long paymentDue) {
        this.paymentDue = paymentDue;
    }

    public List<ItemDto> getItems() {
        return items;
    }

    public void setItems(List<ItemDto> items) {
        this.items = items;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public boolean isSentForPaymentVerification() {
        return sentForPaymentVerification;
    }

    public void setSentForPaymentVerification(boolean sentForPaymentVerification) {
        this.sentForPaymentVerification = sentForPaymentVerification;
    }

    public boolean isFirstPaymentApproved() {
        return firstPaymentApproved;
    }

    public void setFirstPaymentApproved(boolean firstPaymentApproved) {
        this.firstPaymentApproved = firstPaymentApproved;
    }

    public boolean isApprovedByInventoryManager() {
        return approvedByInventoryManager;
    }

    public void setApprovedByInventoryManager(boolean approvedByInventoryManager) {
        this.approvedByInventoryManager = approvedByInventoryManager;
    }

    public boolean isPaymentClear() {
        return paymentClear;
    }

    public void setPaymentClear(boolean paymentClear) {
        this.paymentClear = paymentClear;
    }

    public boolean isReceivedByUser() {
        return receivedByUser;
    }

    public void setReceivedByUser(boolean receivedByUser) {
        this.receivedByUser = receivedByUser;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getApprovedByDeliveryIncharge() {
        return approvedByDeliveryIncharge;
    }

    public void setApprovedByDeliveryIncharge(Boolean approvedByDeliveryIncharge) {
        this.approvedByDeliveryIncharge = approvedByDeliveryIncharge;
    }
}
