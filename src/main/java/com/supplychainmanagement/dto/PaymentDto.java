package com.supplychainmanagement.dto;

import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class PaymentDto implements Serializable {

    long id;
    int paidAmount;
    private String accountNumber;
    private RequisitionDto requisition;
    private UserDto user;
    private String transactionId;
    private  boolean receivedStatus = false;
    private String paymentMethod;
    private boolean duePayment;
    private LocalDateTime createdAt;

    public PaymentDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(int paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public RequisitionDto getRequisition() {
        return requisition;
    }

    public void setRequisition(RequisitionDto requisition) {
        this.requisition = requisition;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public boolean isReceivedStatus() {
        return receivedStatus;
    }

    public void setReceivedStatus(boolean receivedStatus) {
        this.receivedStatus = receivedStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isDuePayment() {
        return duePayment;
    }

    public void setDuePayment(boolean duePayment) {
        this.duePayment = duePayment;
    }
}
