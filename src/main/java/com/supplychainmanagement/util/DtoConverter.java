package com.supplychainmanagement.util;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
public class DtoConverter {



    public static <D, E> List<D> ConvertToDtoList(List<E> entityList , List<D> dtoList) {
         ModelMapper modelMapper = new ModelMapper();
        entityList.forEach(entity->{
                  D d = modelMapper.map(entity , (Type) dtoList.getClass());
                  dtoList.add(d);
                    });
        return dtoList;


    }
}
