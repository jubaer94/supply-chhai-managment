package com.supplychainmanagement.restapi;

import com.supplychainmanagement.dto.CategoryDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.service.CategoryService;
import com.supplychainmanagement.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class apiController {

    private final CategoryService categoryService;

    private final ProductService productService ;

    public apiController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping(value = "/category-products/{id}", consumes = "application/json", produces = "application/json")
    public @ResponseBody List<ProductDto> getCategories(@PathVariable("id") Long id1)
    {
        CategoryDto category  = categoryService.findById(id1);
       List<ProductDto> products = category.getProducts();

       return products;


    }
    @GetMapping(value = "/product/{id}", consumes = "application/json", produces = "application/json")
    public @ResponseBody ProductDto getProduct(@PathVariable("id") Long id1)
    {
        ProductDto product = productService.findByid(id1);

        return product;
    }
}
