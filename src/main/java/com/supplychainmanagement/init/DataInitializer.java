package com.supplychainmanagement.init;

import com.supplychainmanagement.model.Role;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.repository.RoleRepository;
import com.supplychainmanagement.repository.UserRepository;
import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializer implements CommandLineRunner {

  private final   UserRepository userRepository;
  private final RoleRepository roleRepository ;
  private final BCryptPasswordEncoder encoder;

  @Autowired
    public DataInitializer(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
      this.encoder = encoder;
  }

    @Override
    public void run(String... args) throws Exception {


        System.out.println("hello from init");

        List<String> roles = Arrays.asList("admin" , "accounts_manager", "inventory_manager" , "delivery_incharge" ,"dealer");
        String password = encoder.encode("12345");

        roles.forEach(role->{
            if (!roleRepository.existsByAuthority(role)) {
                Role role1 = roleRepository.save(new Role(role));
                List<Role> roles1 = new ArrayList<>();
                roles1.add(role1);
                User user = new User(role,password,role+"123@gamil.com");
                user.setRoles(roles1);
                if (!userRepository.existsUsersByUsername(user.getUsername()))
                {
                    User user1 = userRepository.save(user);
                    System.out.println(user1.getUsername()+" saved ");
                }

            }
        });



//        String password = encoder.encode("12345");
//
//        Role role = roleRepository.save(new Role("accounts_manager"));
//        List<Role> roles = new ArrayList<>();
//        roles.add(role);
//        User user = new User("accounts_manager",password,"accountmanager@gmail.com");
//        user.setRoles(roles);
//        userRepository.save(user);
//        System.out.println(user.getUsername()+" saved ");
//
//        Role role1 = roleRepository.save(new Role("inventory_manager"));
//        List<Role> roles1 = new ArrayList<>();
//        roles1.add(role1);
//        User user1 = new User("inventory_manager",password,"inventorymanager@gmail.com");
//        user1.setRoles(roles1);
//        userRepository.save(user1);
//        System.out.println(user1.getUsername()+" saved ");
//
//        Role role2 = roleRepository.save(new Role("delivery_incharge"));
//        List<Role> roles2 = new ArrayList<>();
//        roles2.add(role2);
//        User user2 = new User("delivery_incharge",password,"deliveryincahrge@gmail.com");
//        user2.setRoles(roles2);
//        userRepository.save(user2);
//        System.out.println(user2.getUsername()+" saved ");
//
//        Role role3 = roleRepository.save(new Role("admin"));
//        List<Role> roles3 = new ArrayList<>();
//        roles3.add(role3);
//        User user3 = new User("admin",password,"admin@gmail.com");
//        user3.setRoles(roles3);
//        userRepository.save(user3);
//        System.out.println(user3.getUsername()+" saved ");










    }
}
