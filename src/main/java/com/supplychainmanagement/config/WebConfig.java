package com.supplychainmanagement.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
            "classpath:/META-INF/resources/", "classpath:/resources/",
            "classpath:/static/", "classpath:/public/" };

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/**")
                .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);


        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("/webjars/");

        Path productUploadImageDirectory = Paths.get("./uploaded-image");
        String productImageUploadPath = productUploadImageDirectory.toFile().getAbsolutePath();
        registry.addResourceHandler("/uploaded-image/**").addResourceLocations("file:"+productImageUploadPath+"/");

    }

}
