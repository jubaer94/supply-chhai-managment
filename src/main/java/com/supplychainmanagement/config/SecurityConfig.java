package com.supplychainmanagement.config;

import com.supplychainmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService ;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public SecurityConfig(UserService userService, BCryptPasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().disable()
                .csrf().disable().authorizeRequests()
                .antMatchers("/").hasAnyAuthority("dealer" , "admin" , "accounts_manager" , "inventory_manager" ,"delivery_incharge")
                .antMatchers("/category/**").hasAnyAuthority("admin")
                .antMatchers("/wishlist/**").hasAnyAuthority("admin" ,"dealer" ,"accounts_manager")
                .antMatchers("/product/**").hasAnyAuthority("admin")
                .antMatchers("/resources/**", "/temp/**").permitAll()
                .antMatchers("/test/**").permitAll()
                .antMatchers("admin/**").hasAnyAuthority("admin")
                .antMatchers("/approval/**").hasAnyAuthority("inventory_manager" ,"accounts_manager" ,"delivery_incharge")
                .antMatchers("/api/**").permitAll()
                .antMatchers("/payment/**").hasAnyAuthority("admin","dealer" ,"accounts_manager")
                .antMatchers("/dealer/**").hasAnyAuthority("admin","dealer" ,"accounts_manager")
                .antMatchers("/rest/**").permitAll()
                .antMatchers("/home/**").permitAll() .and().authorizeRequests().anyRequest().permitAll()

        ;

        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login") ;;
        http.formLogin().loginPage("/login")
                .loginProcessingUrl("/login-processing").permitAll();


        http.logout().logoutUrl("/logout");
    }

}
