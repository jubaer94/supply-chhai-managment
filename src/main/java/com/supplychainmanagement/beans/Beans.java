package com.supplychainmanagement.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class Beans {
    @Bean
    BCryptPasswordEncoder BCPasswordEncoder(){
        return new BCryptPasswordEncoder(11);
    }
}
