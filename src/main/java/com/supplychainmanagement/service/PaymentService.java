package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.PaymentDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.model.Payment;

import java.io.IOException;
import java.util.List;

public interface PaymentService {
    public List<PaymentDto> findAll();

    public PaymentDto findByid(long theId);

    public PaymentDto save(PaymentDto paymentDto) throws IOException;

    public void deleteById(long theId);

    public List<PaymentDto> findAllUnApprovedPayment();
    public List<PaymentDto> findAllUnApprovedDuePayments();
    public PaymentDto saveDue(PaymentDto paymentDto);
    public PaymentDto approvePayment(PaymentDto paymentDto);

}
