package com.supplychainmanagement.service;

import com.supplychainmanagement.model.Image;

import java.util.List;

public interface ImageService {
    public List<Image> findAll();

    public Image findByid(long theId);

    public Image save(Image image);

    public void deleteById(long theId);
}
