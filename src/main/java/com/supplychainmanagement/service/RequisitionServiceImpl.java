package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.RequisitionDto;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.repository.RequisitionRepository;
import com.supplychainmanagement.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
@Service
public class RequisitionServiceImpl implements RequisitionService {

    private final RequisitionRepository requisitionRepository;
    private final UserRepository userRepository;
    private ModelMapper modelMapper = new ModelMapper();
    @Autowired
    public RequisitionServiceImpl(RequisitionRepository requisitionRepository, UserRepository userRepository) {
        this.requisitionRepository = requisitionRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<RequisitionDto> findAll() {
         List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAll();
        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });
        return requisitionDtos;
    }

    @Override
    public RequisitionDto findByid(long theId) {
        Requisition requisition = requisitionRepository.getOne(theId);
        RequisitionDto requisitionDto = modelMapper.map(requisition , RequisitionDto.class);

        return requisitionDto;
    }

    @Override
    public RequisitionDto save(RequisitionDto requisitionDto) throws IOException {

        if (requisitionDto.getId()<=0)
        {
            Requisition requisition = modelMapper.map(requisitionDto ,Requisition.class);
            User user = userRepository.findByUsername(requisitionDto.getUser().getUsername());
            requisition.setUser(user);
            requisition.generateReferenceId();
            Requisition savedRequisition = requisitionRepository.save(requisition);

            RequisitionDto requisitionDto1 = modelMapper.map(savedRequisition ,RequisitionDto.class);
            return requisitionDto1;

        }
        else {
            Requisition requisition = modelMapper.map(requisitionDto , Requisition.class);
            Requisition savedRequisition = requisitionRepository.save(requisition);

            RequisitionDto requisitionDto1 = modelMapper.map(savedRequisition ,RequisitionDto.class);
            return requisitionDto1;
        }


    }

    @Override
    public void deleteById(long theId) {
            Requisition requisition = requisitionRepository.getOne(theId);
            requisition.setEnabled(false);
            requisitionRepository.save(requisition);
    }

    @Override
    public List<RequisitionDto> firstPaymentUnApproved() {
        return null;
    }

    @Override
    public List<RequisitionDto> firstPaymentApproved() {
        return null;
    }

    @Override
    public List<RequisitionDto> approvedByInventoryManager() {
        return null;
    }

    @Override
    public List<RequisitionDto> approvedByDeliveryIncharge() {
        return null;
    }

    @Override
    public List<RequisitionDto> receivedByUser() {
        return null;
    }

    @Override
    public List<RequisitionDto> findAllRequisitionUnApprovedByUser(String userName, Boolean recivedStatus) {
        return null;
    }


    @Override
    public RequisitionDto findByReferenceId(String referenceId) {
        Requisition requisition = requisitionRepository.findByReferenceId(referenceId);

        if (requisition!=null)
        {
            RequisitionDto requisitionDto = modelMapper.map(requisition, RequisitionDto.class);
            return requisitionDto;
        }
        else {
            RequisitionDto requisition1 = new RequisitionDto();

            return requisition1;

        }

    }
}
