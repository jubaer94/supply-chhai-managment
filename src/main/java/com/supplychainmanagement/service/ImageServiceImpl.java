package com.supplychainmanagement.service;

import com.supplychainmanagement.model.Image;
import com.supplychainmanagement.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ImageServiceImpl implements ImageService {
    private ImageRepository imageRepository;


    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public List<Image> findAll() {
        return imageRepository.findAll();

    }

    @Override
    public Image findByid(long theId) {
        return imageRepository.getOne(theId);
    }

    @Override
    public Image save(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public void deleteById(long theId) {
        imageRepository.deleteById(theId);

    }
}
