package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.ImageDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.model.Image;
import com.supplychainmanagement.model.Product;
import com.supplychainmanagement.repository.CategoryRepository;
import com.supplychainmanagement.repository.ImageRepository;
import com.supplychainmanagement.repository.ProductRepository;
import com.supplychainmanagement.util.FileHandler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService{
    private final ProductRepository productRepository;
    private final ImageRepository imageRepository;
    private final CategoryRepository categoryRepository;
    private ModelMapper modelMapper = new ModelMapper();

    public ProductServiceImpl(ProductRepository productRepository, ImageRepository imageRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.imageRepository = imageRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<ProductDto> findAll() {

        List<Product> products =  productRepository.findAllByEnabledTrue();
        List<ProductDto> productDtos = new ArrayList<>();
        if (products.size()>0)
        {
            products.forEach(o->{
                ProductDto productDto = modelMapper.map(o , ProductDto.class);
                productDtos.add(productDto);
            });
        }
        return productDtos;
    }

    @Override
    public ProductDto findByid(long theId) {

        Product product = productRepository.getOne(theId);
        ProductDto productDto = modelMapper.map(product , ProductDto.class);

        return productDto;
    }

    @Override
    public void save(ProductDto productDto) throws IOException {

        Product product = modelMapper.map(productDto , Product.class);
        List<ImageDto> imageDtoList = productDto.getImageList();

        if(productDto.getId()<=0) {

            if (productDto.getMultipartFiles().length >= 1) {
                List<Image> imageList = new ArrayList<>();
                for (MultipartFile file : productDto.getMultipartFiles()) {
                    String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
                    Image image = imageRepository.save(new Image(fileName));
                    imageList.add(image);
                }
                product.setImageList(imageList);
                Product savedProduct = productRepository.save(product);
                String uploadDir = "./uploaded-image/product/" + savedProduct.getId();
//			String uploadDir = "src/main/resources/uploaded-image/product/" + savedProduct.getId();
                for (MultipartFile file : productDto.getMultipartFiles()) {
                    FileHandler.filUpload(uploadDir, file);
                }


                //return savedProduct;
            }
            else {
                Product product1 = modelMapper.map(productDto , Product.class);
                productRepository.save(product);
            }

        }
        else
        {
            if (productDto.getId() > 0) {

                Optional<Product> result = productRepository.findById(productDto.getId());
                System.out.println("to string  = "+ productDto.getMultipartFiles().getClass().getName());
                if (productDto.getMultipartFiles().length >0) {
                    List<Image> imageList = new ArrayList<>();
                    System.out.println(productDto.getMultipartFiles().length);
                    for (MultipartFile file : productDto.getMultipartFiles()) {
                        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
                        Image image = imageRepository.save(new Image(fileName));
                        imageList.add(image);
                    }
                    Product product1 = result.get();
                    product1.setImageList(imageList);
                    product1.setName(productDto.getName());
                    product1.setCategory(categoryRepository.getOne(productDto.getCategory().getId()));
                    product1.setInStock(productDto.getInStock());
                    product1.setPrice(productDto.getPrice());
                    product1.setQuantity_per_carton(productDto.getQuantity_per_carton());
                    productRepository.save(product1);
                    String uploadDir = "./uploaded-image/product/" + product1.getId();
//			String uploadDir = "src/main/resources/uploaded-image/product/" + savedProduct.getId();
                    for (MultipartFile file : productDto.getMultipartFiles()) {
                        FileHandler.filUpload(uploadDir, file);
                    }

                }
                else {
                    System.out.println("hello from length = 0");
                        Product product1 = result.get();
                        List<Image> images = product1.getImageList();
                        images.forEach(o->{
                        System.out.println(o.getName());
                    });
                        product1.setName(productDto.getName());
                        product1.setCategory(categoryRepository.getOne(productDto.getCategory().getId()));
                        product1.setInStock(productDto.getInStock());
                        product1.setPrice(productDto.getPrice());
                        productRepository.save(product1);

                }
            }



        }


    }

    @Override
    public void deleteById(long theId) {

        Product product = productRepository.getOne(theId);

        product.setEnabled(false);
        productRepository.save(product);

    }
}
