package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.model.Image;
import com.supplychainmanagement.model.Role;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.repository.ImageRepository;
import com.supplychainmanagement.repository.RoleRepository;
import com.supplychainmanagement.repository.UserRepository;
import com.supplychainmanagement.util.FileHandler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder encoder;
    private final ImageRepository imageRepository;

    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder encoder, ImageRepository imageRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.imageRepository = imageRepository;
    }

    @Override
    public List<UserDto> findAll() {
        List<User> users = userRepository.findAll();
        List<UserDto> userDtos = new ArrayList<>();
        users.forEach(o->{
            UserDto userDto = modelMapper.map(o , UserDto.class);
            userDtos.add(userDto);
        });
        return userDtos;
    }

    @Override
    public UserDto findById(long theId) {

        User user = userRepository.getOne(theId);

        UserDto userDto = modelMapper.map(user , UserDto.class);

        return userDto;

    }

    @Override
    public UserDto save(UserDto userDto) throws IOException {

        if (userDto.getId()<=0) {
            User user1 = modelMapper.map(userDto, User.class);
            List<Role> roles = new ArrayList<>();
            List<Image> imageList = new ArrayList<>();
            if (userDto.getMultipartFiles().length >= 1) {
                for (MultipartFile file : userDto.getMultipartFiles()) {
                    String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
                    Image image = imageRepository.save(new Image(fileName));
                    imageList.add(image);
                }
                user1.setImageList(imageList);
            }
            Role role = roleRepository.findByAuthority("dealer");
            roles.add(role);
            user1.setRoles(roles);
            user1.setEnabled(false);
//        user1.setEnabled(false);
            user1.setPassword(encoder.encode(user1.getPassword()));

            User savedUser = userRepository.save(user1);
            String uploadDir = "./uploaded-image/user/" + savedUser.getId();
            if (userDto.getMultipartFiles().length >= 1) {
                for (MultipartFile file : userDto.getMultipartFiles()) {
                    FileHandler.filUpload(uploadDir, file);
                }
            }
            UserDto savedUserDto = modelMapper.map(savedUser ,UserDto.class);

            return savedUserDto;
        }
        else {
            User user = modelMapper.map(userDto , User.class);
            User savedUser = userRepository.save(user);
            UserDto savedUserDto = modelMapper.map(savedUser ,UserDto.class);
            return savedUserDto;
        }
    }


    @Override
    public void deleteById(long theId) {

        User user = userRepository.getOne(theId);
        user.setEnabled(false);

        userRepository.save(user);

    }

    @Override
    public void enableUserById(long id) {
        User user = userRepository.getOne(id);
        user.setEnabled(true);
        userRepository.save(user);
    }

    @Override
    public List<UserDto> findAllInactiveUsers() {
        List<User> users = userRepository.findAllByEnabled(false);
        List<UserDto> userDtos = new ArrayList<>();
        if (users.size()>0) {
            users.forEach(o -> {
                UserDto userDto = modelMapper.map(o, UserDto.class);
                userDtos.add(userDto);
            });
        }
        return userDtos;
    }

    @Override
    public UserDto findByUserName(String userName) {
        User user = userRepository.findByUsername(userName);
        UserDto userDto = modelMapper.map(user , UserDto.class);
        return userDto;
    }

    @Override
    public void setUserDue(String username, long amount, boolean paymentClearStatus) {
        User user = userRepository.findByUsername(username);
        user.setPaymentDue(user.getPaymentDue()+amount);
        user.setPaymentClearStatus(false);
        userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);

        UserDto userDto = modelMapper.map(user , UserDto.class);

        return userDto;

    }


}
