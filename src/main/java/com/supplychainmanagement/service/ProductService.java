package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.ProductDto;

import java.io.IOException;
import java.util.List;

public interface ProductService {

    public List<ProductDto> findAll();

    public ProductDto findByid(long theId);

    public void save(ProductDto productDto) throws IOException;

    public void deleteById(long theId);
}
