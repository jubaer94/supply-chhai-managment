package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.ItemDto;
import com.supplychainmanagement.dto.ProductDto;

import java.io.IOException;
import java.util.List;

public interface ItemService {
    public List<ItemDto> findAll();

    public ItemDto findByid(long theId);

    public ItemDto save(ItemDto itemDto) throws IOException;

    public void deleteById(long theId);
}
