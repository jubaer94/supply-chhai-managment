package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.CategoryDto;
import com.supplychainmanagement.model.Category;
import com.supplychainmanagement.repository.CategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    ModelMapper modelMapper = new ModelMapper();

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDto> findAll() {

        List<Category> categoryList = categoryRepository.findAllByEnabledTrue();
        List<CategoryDto> categoryDtos = new ArrayList<>();
        if (categoryList.size()>0)
        {
            categoryList.forEach(
                    o-> {
                CategoryDto categoryDto = modelMapper.map(o , CategoryDto.class);
                categoryDtos.add(categoryDto);
            });
        }
        return categoryDtos;
    }

    @Override
    public CategoryDto findById(long theId) {

        Category category = categoryRepository.getOne(theId);
         CategoryDto categoryDto = modelMapper.map(category , CategoryDto.class);

        return categoryDto;
    }

    @Override
    public void save(CategoryDto categoryDto) {

        if (categoryDto.getId()<=0) {
            Category category = modelMapper.map(categoryDto, Category.class);
            categoryRepository.save(category);
        }
        else {
            Category category = categoryRepository.getOne(categoryDto.getId());
            category.setName(categoryDto.getName());
            categoryRepository.save(category);
        }

    }

    @Override
    public void deleteById(long theId) {

        Category category = categoryRepository.getOne(theId);
        category.setEnabled(false);
        categoryRepository.save(category);

    }
}
