package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    public List<CategoryDto> findAll();

    public CategoryDto findById(long theId);

    public void save(CategoryDto categoryDto);

    public void deleteById(long theId);
}
