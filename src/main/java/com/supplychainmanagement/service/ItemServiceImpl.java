package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.ItemDto;
import com.supplychainmanagement.model.Item;
import com.supplychainmanagement.repository.ItemRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
@Service
public class ItemServiceImpl implements ItemService {
    private final ItemRepository itemRepository;

    ModelMapper modelMapper = new ModelMapper();

    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public List<ItemDto> findAll() {
        List<Item> items = itemRepository.findAll();
        List<ItemDto> itemDtos = new ArrayList<>();

        items.forEach(o->{
            ItemDto itemDto = modelMapper.map(o ,ItemDto.class);
            itemDtos.add(itemDto);
        });

        return itemDtos;
    }

    @Override
    public ItemDto findByid(long theId) {
        Item item = itemRepository.getOne(theId);
        ItemDto itemDto = modelMapper.map(item , ItemDto.class);

        return itemDto;
    }

    @Override
    public ItemDto save(ItemDto itemDto) throws IOException {
        Item item = modelMapper.map(itemDto , Item.class);

        Item item1 = itemRepository.save(item);
        ItemDto itemDto1 = modelMapper.map(item1 , ItemDto.class);
        return itemDto1 ;
    }

    @Override
    public void deleteById(long theId) {


    }
}
