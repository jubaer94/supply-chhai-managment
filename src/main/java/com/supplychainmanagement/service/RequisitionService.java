package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.PaymentDto;
import com.supplychainmanagement.dto.RequisitionDto;
import com.supplychainmanagement.model.Requisition;

import java.io.IOException;
import java.util.List;

public interface RequisitionService {
    public List<RequisitionDto> findAll();

    public RequisitionDto findByid(long theId);

    public RequisitionDto save(RequisitionDto requisitionDto) throws IOException;
    public void deleteById(long theId);
    public List<RequisitionDto> firstPaymentUnApproved();
    public List<RequisitionDto> firstPaymentApproved();
    public List<RequisitionDto> approvedByInventoryManager();
    public List<RequisitionDto> approvedByDeliveryIncharge();
    public List<RequisitionDto> receivedByUser();
    public List<RequisitionDto> findAllRequisitionUnApprovedByUser(String userName , Boolean recivedStatus);
    RequisitionDto findByReferenceId(String referenceId);
}
