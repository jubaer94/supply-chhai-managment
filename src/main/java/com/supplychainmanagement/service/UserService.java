package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;


public interface UserService extends UserDetailsService {

    public List<UserDto> findAll();

    public UserDto findById(long theId);

    public UserDto save(UserDto user) throws IOException;

    public void deleteById(long theId);

    public void enableUserById(long id);

    public List<UserDto> findAllInactiveUsers();
    public UserDto findByUserName(String userName);
    public void setUserDue(String username , long amount , boolean paymentClearStatus);


}
