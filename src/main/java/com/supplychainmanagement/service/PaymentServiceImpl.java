package com.supplychainmanagement.service;

import com.supplychainmanagement.dto.PaymentDto;
import com.supplychainmanagement.model.Payment;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.repository.PaymentRepository;
import com.supplychainmanagement.repository.RequisitionRepository;
import com.supplychainmanagement.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
@Service
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository paymentRepository;
    private final UserRepository userRepository;
    private final RequisitionRepository requisitionRepository;
    private ModelMapper modelMapper = new ModelMapper();

    public PaymentServiceImpl(PaymentRepository paymentRepository, UserRepository userRepository, RequisitionRepository requisitionRepository) {
        this.paymentRepository = paymentRepository;
        this.userRepository = userRepository;
        this.requisitionRepository = requisitionRepository;
    }

    @Override
    public List<PaymentDto> findAll() {
        List<Payment> payments = paymentRepository.findAll();
        List<PaymentDto> paymentDtos = new ArrayList<>();
        payments.forEach(o -> {
            PaymentDto paymentDto = modelMapper.map(o, PaymentDto.class);
            paymentDtos.add(paymentDto);
        });
        return paymentDtos;
    }

    @Override
    public PaymentDto findByid(long theId) {
        Payment payment = paymentRepository.getOne(theId);
        PaymentDto paymentDto = modelMapper.map(payment, PaymentDto.class);
        return paymentDto;
    }

    @Override
    public PaymentDto save(PaymentDto paymentDto) throws IOException {

        Payment payment = modelMapper.map(paymentDto, Payment.class);
        User user = userRepository.findByUsername(paymentDto.getUser().getUsername());
        payment.setUser(user);
        Payment savedPayment = paymentRepository.save(payment);

        PaymentDto paymentDto1 = modelMapper.map(savedPayment, PaymentDto.class);
        return paymentDto1;

    }

    @Override
    public void deleteById(long theId) {

    }

    @Override
    public List<PaymentDto> findAllUnApprovedPayment() {
        List<Payment> payments = paymentRepository.findAllByReceivedStatus(false);
        List<PaymentDto> paymentDtos = new ArrayList<>();
        payments.forEach(o -> {
            PaymentDto paymentDto = modelMapper.map(o, PaymentDto.class);
            paymentDtos.add(paymentDto);
        });
        return paymentDtos;
    }

    @Override
    public List<PaymentDto> findAllUnApprovedDuePayments() {
        List<Payment> payments = paymentRepository.findAllByReceivedStatusAndDuePayment(false , true);
        List<PaymentDto> paymentDtos = new ArrayList<>();
        payments.forEach(o -> {
            PaymentDto paymentDto = modelMapper.map(o, PaymentDto.class);
            paymentDtos.add(paymentDto);
        });
        return paymentDtos;
    }

    @Override
    public PaymentDto saveDue(PaymentDto paymentDto) {
        paymentDto.setDuePayment(true);
        Requisition requisition = requisitionRepository.findByReferenceId(paymentDto.getRequisition().getReferenceId());
        User user = userRepository.findByUsername(paymentDto.getUser().getUsername());
        Payment payment = modelMapper.map(paymentDto, Payment.class);

        payment.setRequisition(requisition);
        payment.setUser(user);
        paymentRepository.save(payment);
        System.out.println("due payment saved successfully ");

        PaymentDto paymentDto1 = modelMapper.map(payment, PaymentDto.class);

        return paymentDto1;

    }

    @Override
    public PaymentDto approvePayment(PaymentDto paymentDto) {
        Requisition requisition = requisitionRepository.findByReferenceId(paymentDto.getRequisition().getReferenceId());
        User user = userRepository.findByUsername(paymentDto.getUser().getUsername());
        if (paymentDto.getPaidAmount() >= requisition.getPaymentDue())
        {

            requisition.setPaymentDue(0);
            requisition.setPaymentClear(true);
            requisitionRepository.save(requisition);


        }
        else {
            requisition.setPaymentDue(requisition.getPaymentDue() - paymentDto.getPaidAmount());
            requisitionRepository.save(requisition);
        }

        if (paymentDto.getPaidAmount() >= user.getPaymentDue())
        {
            user.setPaymentDue(0);
            user.setPaymentClearStatus(true);
            userRepository.save(user);
        }
        else {
            user.setPaymentDue(user.getPaymentDue() - paymentDto.getPaidAmount());
            user.setPaymentClearStatus(false);
        }
        Payment payment = paymentRepository.getOne(paymentDto.getId());
        payment.setReceivedStatus(true);
      Payment payment1 =  paymentRepository.save(payment);
      PaymentDto paymentDto1 = modelMapper.map(payment1 , PaymentDto.class);
        return paymentDto1;
    }
}