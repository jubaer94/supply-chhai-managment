package com.supplychainmanagement.repository;

import com.supplychainmanagement.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment , Long> {

    List<Payment> findAllByReceivedStatus(boolean receivedStatus);

    List<Payment> findAllByReceivedStatusAndDuePayment(boolean receivedStatus, boolean duePayment);
}
