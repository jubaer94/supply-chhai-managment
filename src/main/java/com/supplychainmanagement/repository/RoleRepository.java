package com.supplychainmanagement.repository;

import com.supplychainmanagement.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role , Long> {
    Role findByAuthority(String authority);
    Boolean existsByAuthority(String authority);

}