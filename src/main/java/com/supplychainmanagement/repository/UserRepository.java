package com.supplychainmanagement.repository;

import com.supplychainmanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User , Long> {

      User findByUsername(String username);
      List<User> findByEmail(String email);
      List<User> findAllByEnabled(boolean enabled);
      Boolean existsUsersByUsername(String username);
      Boolean existsUsersByEmail(String email);
}
