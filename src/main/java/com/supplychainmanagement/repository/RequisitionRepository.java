package com.supplychainmanagement.repository;

import com.supplychainmanagement.model.Requisition;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RequisitionRepository extends JpaRepository<Requisition , Long> {


    Requisition findByReferenceId(String referenceId);
    List<Requisition> findAllBySentForPaymentVerification(boolean sentForPaymentVerification);
    List<Requisition> findAllBySentForPaymentVerificationAndFirstPaymentApproved(boolean sentForPaymentVerification, boolean firstPaymentApproved);
    List<Requisition> findAllByFirstPaymentApprovedAndApprovedByInventoryManager(boolean firstPaymentApproved, boolean approvedByInventoryManager);
    List<Requisition> findAllByApprovedByInventoryManagerAndApprovedByDeliveryIncharge(boolean approvedByInventoryManager, Boolean approvedByDeliveryIncharge);
    List<Requisition> findAllByApprovedByDeliveryInchargeAndReceivedByUser(Boolean approvedByDeliveryIncharge, boolean receivedByUser);
    List<Requisition> findAllByUserUsernameAndReceivedByUser(String user_username, boolean receivedByUser);

}
