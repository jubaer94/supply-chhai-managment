package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.repository.UserRepository;
import com.supplychainmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;

@Controller
public class RegisterController {

    private final UserService userService ;
    private final UserRepository userRepository;

    @Autowired
    public RegisterController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @GetMapping("/dealer/register-page")
    public  String showRegisterPage(Model model)
    {
        UserDto user = new UserDto();
        model.addAttribute("user" , user);
        return "login/registration";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute UserDto user , @RequestParam("fileImages") MultipartFile[] multipartFiles, Model model , RedirectAttributes redirectAttributes) throws IOException {

        user.setMultipartFiles(multipartFiles);
        if (userRepository.existsUsersByUsername(user.getUsername()))
        {
            String error="username taken provide a different user name";
            redirectAttributes.addFlashAttribute("message" ,error);
            redirectAttributes.addFlashAttribute(error);
            return "redirect:/dealer/register-page";


        }
        else if (userRepository.existsUsersByEmail(user.getEmail()))
        {
            String error = "this email is already used  provide a different email address";
           model.addAttribute("message" ,error);

            return "redirect:/register-page";
        }
        else {
            userService.save(user);

            System.out.println("from register controller user saved");
            String message = "user registered successfully";

            model.addAttribute("message" , message);

            return "login/login";
        }


    }
}
