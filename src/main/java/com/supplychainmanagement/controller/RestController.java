package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.CategoryDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.service.CategoryService;
import com.supplychainmanagement.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@RequestMapping("/rest")
@org.springframework.web.bind.annotation.RestController
public class RestController {
    private final  CategoryService categoryService;
    private final ProductService productService;

    public RestController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping("/category-products/{id}")
    public List<ProductDto> categories(@PathVariable long id)
    {

        CategoryDto categoryDto = categoryService.findById(id);
        List<ProductDto> productDtos = categoryDto.getProducts();

        return productDtos;



    }

    @GetMapping("/product/{id}")
    public ProductDto getProduct(@PathVariable long id)
    {


         ProductDto productDto = productService.findByid(id);

        return productDto;



    }

}
