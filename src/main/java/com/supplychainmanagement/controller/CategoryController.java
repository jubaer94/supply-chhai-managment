package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.CategoryDto;
import com.supplychainmanagement.model.Category;
import com.supplychainmanagement.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/category")
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/list")
    public String showAll( Model model)
    {
        List<CategoryDto> categoryDtos = categoryService.findAll();

        model.addAttribute("categories" , categoryDtos);

        return "category/category-list";

    }

    @GetMapping("/add")
    public String showForm(Model model)
    {
        CategoryDto categoryDto = new CategoryDto();
        model.addAttribute("category" , categoryDto);

        return "category/category-form";

    }
    @PostMapping("/add")
    public String save(@ModelAttribute CategoryDto categoryDto )
    {
        categoryService.save(categoryDto);
        return "redirect:/category/list";
    }

    @GetMapping("edit/{id}")
     public String edit(Model model , @PathVariable("id") long id )
    {
        CategoryDto categoryDto = categoryService.findById(id);

        model.addAttribute("category" , categoryDto);
        return "category/category-form";
    }
    @GetMapping("delete/{id}")
    public String delete(Model model , @PathVariable("id") long id)
    {
        categoryService.deleteById(id);
        return "redirect:/category/list";
    }
}
