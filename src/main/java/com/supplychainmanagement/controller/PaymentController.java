package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.ItemDto;
import com.supplychainmanagement.dto.PaymentDto;
import com.supplychainmanagement.dto.RequisitionDto;
import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.service.ItemService;
import com.supplychainmanagement.service.PaymentService;
import com.supplychainmanagement.service.RequisitionService;
import com.supplychainmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/payment")
public class PaymentController {
    private final UserService userService;
    private final RequisitionService requisitionService;
    private final PaymentService paymentService;
    private final ItemService itemService;

    @Autowired
    public PaymentController(UserService userService, RequisitionService requisitionService, PaymentService paymentService, ItemService itemService) {
        this.userService = userService;
        this.requisitionService = requisitionService;
        this.paymentService = paymentService;
        this.itemService = itemService;
    }

    @GetMapping("/add")
    public String showPaymentForm(Model model)
    {
        PaymentDto paymentDto = new PaymentDto();
        model.addAttribute("payment" , paymentDto);
        return "payment/payment-form";
    }
    @GetMapping("/due")
    public String showDuePaymentForm(Model model)
    {
        PaymentDto paymentDto = new PaymentDto();
        model.addAttribute("payment" , paymentDto);
        return "payment/due-payment-form";
    }
    @PostMapping("/due/add")
    public String saveDuePayment(@ModelAttribute PaymentDto paymentDto , Principal principal , Model model) throws IOException {
        UserDto userDto = userService.findByUserName(principal.getName());
        RequisitionDto requisition = requisitionService.findByReferenceId(paymentDto.getRequisition().getReferenceId());
        paymentDto.setUser(userDto);
        paymentDto.setRequisition(requisition);
        if (requisition.getId()>0)
        {
            paymentService.saveDue(paymentDto);
            model.addAttribute("message" , "payment saved successfully");
            System.out.println("payment saved");
            return "redirect:/payment/due";

        }
        else {
            model.addAttribute("message" , "no requisition found with reference id ");
            System.out.println("payment not saved");
            return "redirect:/payment/due";
        }


    }
    @PostMapping("/add")
    public String savePayment(@ModelAttribute PaymentDto paymentDto , Model model , HttpSession session , Principal principal) throws IOException {

        if (session.getAttribute("items")!=null) {
            UserDto userDto = userService.findByUserName(principal.getName());
            paymentDto.setUser(userDto);
            PaymentDto savedPayment = paymentService.save(paymentDto);

            System.out.println(principal.getName());
            RequisitionDto requisitionDto = new RequisitionDto();

            List<ItemDto> itemDtos = new ArrayList<>();
            requisitionDto.setUser(userDto);
            List<PaymentDto> paymentDtos = new ArrayList<>();
            paymentDtos.add(savedPayment);
            requisitionDto.setPayments(paymentDtos);
            long total = (long) session.getAttribute("total");
            requisitionDto.setTotal(total);

//            if (paymentDto.getPaidAmount() < total) {
//                long due = total - paymentDto.getPaidAmount();
//                requisitionDto.setPaymentDue(due);
//                requisitionDto.setPaymentClear(false);
//                requisitionService.save(requisitionDto);
//                userService.setUserDue(principal.getName(), due, false);
//            } else {
//                requisitionDto.setPaymentClear(true);
//                requisitionDto.setPaymentDue(0);
//                requisitionService.save(requisitionDto);
//
//            }


            if (session.getAttribute("items") != null) {
                HashMap<Long, ItemDto> items = (HashMap<Long, ItemDto>) session.getAttribute("items");
                items.values().forEach(itemDto -> {
                    try {
//                        itemDto.setRequisition(requisitionDto);
                        ItemDto itemDto1 = itemService.save(itemDto);
                        itemDtos.add(itemDto1);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                System.out.println(itemDto.getPrice());
                });
            }

            requisitionDto.setItems(itemDtos);
            requisitionDto.setPaymentDue(total);
            requisitionDto.setPaymentClear(false);
            RequisitionDto requisitionDto1=requisitionService.save(requisitionDto);
            userService.setUserDue(principal.getName(),total,false);
        }
        else {
            System.out.println("session items is null");
        }




        return"redirect:/";
    }
    @GetMapping("/pending")
    public String showPendingForApprovalPayments(Model model )
    {
        List<PaymentDto> paymentDtos = paymentService.findAllUnApprovedDuePayments();
        model.addAttribute("payments" , paymentDtos);
        return "accountant/approve-payments";
    }
    @GetMapping("/approve/{id}")
    public String approvePayment(@PathVariable("id") long id, Model model)
    {
        PaymentDto paymentDto = paymentService.findByid(id);

        paymentService.approvePayment(paymentDto);
        return "redirect:/payment/pending";


    }


}
