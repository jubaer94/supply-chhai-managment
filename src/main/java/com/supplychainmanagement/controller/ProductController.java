package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.CategoryDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.service.CategoryService;
import com.supplychainmanagement.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService ;
    private  final CategoryService categoryService;

    public ProductController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @GetMapping("/list")
    public String showAll(Model model )
    {
        List<ProductDto> productDtos = productService.findAll();
        model.addAttribute("products" , productDtos);
        return "product/product-list";
    }

    @GetMapping("/add")
    public String showForm(Model model)
    {
        ProductDto product = new ProductDto();
        List<CategoryDto> categoryDtos = categoryService.findAll();
        model.addAttribute("product" , product);
        model.addAttribute("categories" , categoryDtos);

        return "product/product-form";
    }
    @PostMapping("/add")
    public String saveProduct(@ModelAttribute ProductDto productDto , Model model , @RequestParam("fileImages")MultipartFile[] multipartFiles) throws IOException {
       productDto.setMultipartFiles(multipartFiles);
       productService.save(productDto);
       return "redirect:/product/list";
    }
    @GetMapping("edit/{id}")
    public String edit(Model model , @PathVariable("id") long id )
    {
        ProductDto productDto = productService.findByid(id);
        List<CategoryDto> categoryDtos = categoryService.findAll();
        model.addAttribute("product" , productDto);
        model.addAttribute("categories" , categoryDtos);

        return "product/product-form";
    }
    @GetMapping("delete/{id}")
    public String delete(Model model , @PathVariable("id") long id)
    {
        productService.deleteById(id);
        return "redirect:/product/list";
    }

}
