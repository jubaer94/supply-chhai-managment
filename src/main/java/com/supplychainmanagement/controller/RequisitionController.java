package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.CategoryDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.dto.RequisitionDto;
import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.service.CategoryService;
import com.supplychainmanagement.service.ProductService;
import com.supplychainmanagement.service.RequisitionService;
import com.supplychainmanagement.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/requisition")
public class RequisitionController {

    private  final CategoryService categoryService;
    private final RequisitionService requisitionService;
    private final ProductService productService;
    private final UserService userService;


    public RequisitionController(CategoryService categoryService, RequisitionService requisitionService, ProductService productService, UserService userService) {
        this.categoryService = categoryService;
        this.requisitionService = requisitionService;
        this.productService = productService;
        this.userService = userService;
    }

    @GetMapping("/add-item")
    public String showAddPage(Model model ,Principal principal)
    {

        List<CategoryDto> categoryDtos = categoryService.findAll();
        ProductDto productDto =new ProductDto();
        UserDto userDto = userService.findByUserName(principal.getName());
        model.addAttribute("product" , productDto);
        model.addAttribute("categories" , categoryDtos);
        model.addAttribute("user" , userDto);

        return "wishlist/add-item";

    }
    @GetMapping("/show/{id}")
    public String showRequisition(Model model , @PathVariable("id") long id)
    {
       RequisitionDto requisitionDto =  requisitionService.findByid(id);
       model.addAttribute("requisition" , requisitionDto);
        return "requisition/single-view";
    }
//@GetMapping("/test")
//    public String test( Model model)
//{
//    List<RequisitionDto> requisitionDtos = requisitionService.findAll();
//    requisitionDtos.forEach(requisitionDto -> {
//        System.out.println(requisitionDto.getUser().getUsername());
//        System.out.println(requisitionDto.getReferenceId());
//        requisitionDto.getPayments().forEach(paymentDto -> {
//            System.out.println(paymentDto.getTransactionId());
//        });
//    });
//
//    return "redirect:requisition/add-item";
//}


}
