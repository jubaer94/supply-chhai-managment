package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.ItemDto;
import com.supplychainmanagement.dto.ProductDto;
import com.supplychainmanagement.model.Item;
import com.supplychainmanagement.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.HashMap;

@Controller
@RequestMapping("/cart")
public class CartController {
    private  final ProductService productService;

    public CartController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/add-item")
    public String add(@ModelAttribute ProductDto productDto, HttpSession session , Model model)
    {
//        System.out.println("product id = "+productDto.getId());
//        System.out.println(productDto.getInStock());
        ProductDto product = productService.findByid(productDto.getId());
//        System.out.println("product name = " +product.getName());
        long id = productDto.getId();

        if (session.getAttribute("items") == null) {

            HashMap<Long, ItemDto> items = new HashMap<>();
            ItemDto itemDto = new ItemDto();
            itemDto.setName(product.getName());
            itemDto.setPrice(product.getPrice());
            itemDto.setImageList(product.getImageList());
            itemDto.setProduct_id(product.getId());
            itemDto.setQuantity_per_carton(product.getQuantity_per_carton());
            itemDto.setQuantity(productDto.getInStock());
            items.put(id, itemDto);

            session.setAttribute("items", items);
        } else if (session.getAttribute("items") != null){
            HashMap<Long, ItemDto> items = (HashMap<Long, ItemDto>) session.getAttribute("items");
            if (items.containsKey(id)) {
                int qty = productDto.getInStock();
                ItemDto itemDto = items.get(id);
                itemDto.setQuantity(itemDto.getQuantity()+qty);
                items.remove(id);
                items.put(id , itemDto);

            } else {
                ItemDto itemDto = new ItemDto();
                itemDto.setName(product.getName());
                itemDto.setPrice(product.getPrice());
                itemDto.setImageList(product.getImageList());
                itemDto.setProduct_id(product.getId());
                itemDto.setQuantity_per_carton(product.getQuantity_per_carton());
                itemDto.setQuantity(productDto.getInStock());
                items.put(id, itemDto);
            }
            session.setAttribute("items", items);
        }

        HashMap<Long, ItemDto> items = (HashMap<Long, ItemDto>) session.getAttribute("items");

        int size = 0;
        long total = 0;

        for (ItemDto value : items.values()) {
            total += (value.getQuantity_per_carton()*value.getQuantity())*value.getPrice();
            size += value.getQuantity()*value.getQuantity_per_carton();
        }

//        model.addAttribute("size", size);
       session.setAttribute("total",total);
        session.setAttribute("size" , size);
        
        return "redirect:/requisition/add-item";
    }
    @GetMapping("/clear")
    public String clearCart(HttpSession session , Principal principal)
    {

        session.removeAttribute("items");
        session.removeAttribute("total");
        session.removeAttribute("size");
        System.out.println(principal.getName());
        return "redirect:/requisition/add-item";

    }
}
