package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.PaymentDto;
import com.supplychainmanagement.dto.RequisitionDto;
import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.model.Payment;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.repository.RequisitionRepository;
import com.supplychainmanagement.service.UserService;
import com.supplychainmanagement.util.DtoConverter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
@Controller
@RequestMapping("/dealer")
public class DealerController {

    private final UserService userService;
    private final RequisitionRepository requisitionRepository ;
    private ModelMapper modelMapper = new ModelMapper();

    public DealerController(UserService userService, RequisitionRepository requisitionRepository) {
        this.userService = userService;
        this.requisitionRepository = requisitionRepository;
    }

    @GetMapping("/requisition/all")
    public String showAllRequisitionsOfDealer(Model model , Principal principal)
    {
        UserDto userDto = userService.findByUserName(principal.getName());
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        requisitionDtos = userDto.getRequisitions();
        model.addAttribute("requisitions" , requisitionDtos);
        return "/dealer/dealer-requisitions";
    }

    @GetMapping("/requisition/pending-for-dealer-approval")
    public String showAllUnRequisitionsOfDealer(Model model , Principal principal)
    {

        List<Requisition> requisitions = requisitionRepository.findAllByUserUsernameAndReceivedByUser(principal.getName() , false);
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        requisitions.forEach(requisition -> {
            RequisitionDto requisitionDto = modelMapper.map(requisition , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });
        model.addAttribute("requisitions" , requisitionDtos);


//     List<Requisition> requisitions = requisitionRepository.findAll();
//     List<RequisitionDto> requisitionDtos = new ArrayList<>();
//
//       List<RequisitionDto> requisitionDtos1 = DtoConverter.ConvertToDtoList(requisitions , requisitionDtos);
//       requisitionDtos1.forEach(o->{
//           System.out.println(o.getReferenceId());
//       });



        return "/dealer/unapproved-dealer-requisitions";
    }
    @GetMapping("/requisition/approveRequisition/{id}")
    public String approveRequisitionByDealer(Model model, @PathVariable("id") long id , Principal principal)
    {
        Requisition requisition = requisitionRepository.getOne(id);
        requisition.setReceivedByUser(true);
        requisitionRepository.save(requisition);
        List<Requisition> requisitions = requisitionRepository.findAllByUserUsernameAndReceivedByUser(principal.getName() , false);
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        requisitions.forEach(o -> {
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });
        model.addAttribute("requisitions" , requisitionDtos);
        model.addAttribute("message" ,"requisition approved successfully");
        return "/dealer/unapproved-dealer-requisitions";
    }

    @GetMapping("/payments/all")
    public  String ShowAllPaymentsOfADealer(Model model , Principal principal)
    {
        UserDto userDto = userService.findByUserName(principal.getName());
        List<PaymentDto> paymentDtos = userDto.getPayments();
        model.addAttribute("payments" , paymentDtos);
        return "/dealer/dealer-payments";
    }
}
