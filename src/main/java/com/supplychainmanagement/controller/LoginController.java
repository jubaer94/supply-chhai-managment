package com.supplychainmanagement.controller;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login() {
        return "login/login";
    }

//    @RequestMapping("/login-success")
//    public String loginSuccess() {
//        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println(user.toString());
//        return "redirect:student/list";
//    }


}
