package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.model.Item;
import com.supplychainmanagement.model.Product;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.repository.UserRepository;
import net.bytebuddy.utility.RandomString;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class Home {

    private final UserRepository userRepository;

    @Autowired
    public Home(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/")
    public String showHomePage(Model model)
    {


        return "index";


    }

//    @GetMapping("/test/add")
//    public String addProduct(HttpSession session, Model model)
//    {
//        if (session.getAttribute("cart")==null)
//        {
//            Requisition requisition = new Requisition();
//            List<Item> Items = new ArrayList<>();
//            Item Item = new Item();
//            Item.setName("lux");
//
//            Items.add(Item);
//            requisition.setItems(Items);
//
//            session.setAttribute("cart" ,requisition);
//
//        }
//        else if (session.getAttribute("cart")!=null)
//        {
//            Requisition requisition =(Requisition) session.getAttribute("cart");
//
//            Item item = new Item();
//            item.setName(RandomString.make(10));
////            List<Item> items = new ArrayList<>();
////            items.add(item);
////            requisition.setItems(items);
//            requisition.getItems().add(item);
//            session.setAttribute("cart" ,requisition );
//        }
//        return "index";
//    }
}
