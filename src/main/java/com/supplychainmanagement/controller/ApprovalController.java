package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.RequisitionDto;
import com.supplychainmanagement.model.Product;
import com.supplychainmanagement.model.Requisition;
import com.supplychainmanagement.repository.ProductRepository;
import com.supplychainmanagement.repository.RequisitionRepository;
import com.supplychainmanagement.service.PaymentService;
import com.supplychainmanagement.service.RequisitionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/approval")
public class ApprovalController {
    private final RequisitionRepository requisitionRepository;
    private final ProductRepository productRepository;
    private final RequisitionService requisitionService ;
    private final PaymentService paymentService;
    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public ApprovalController(RequisitionRepository requisitionRepository, ProductRepository productRepository, RequisitionService requisitionService, PaymentService paymentService) {
        this.requisitionRepository = requisitionRepository;
        this.productRepository = productRepository;
        this.requisitionService = requisitionService;
        this.paymentService = paymentService;
    }

    @GetMapping("/requisition/account_manager")
    public String showAccountManagerApprovalRequisitions(Model model)
    {
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllBySentForPaymentVerificationAndFirstPaymentApproved(true,false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/accountant/approve-requisition";
    }


    @GetMapping("/requisition/delivery-incharge/unapprovedRequisitions")
    public String showDeliveryInchargeApprovalRequisitions(Model model)
    {
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllByApprovedByInventoryManagerAndApprovedByDeliveryIncharge(true,false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/delivery-incharge/unapproved-requisition-list";
    }



    @GetMapping("/requisition/delivery-incharge/approve/{id}")
    public String approveRequisitionByDeliveryIncharge(Model model , @PathVariable long id)
    {
        Requisition requisition = requisitionRepository.getOne(id);
        requisition.setApprovedByDeliveryIncharge(true);
        requisitionRepository.save(requisition);
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllByApprovedByInventoryManagerAndApprovedByDeliveryIncharge(true,false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/delivery-incharge/unapproved-requisition-list";
    }



    @GetMapping("/requisition/inventory_manager/first-payment-verification-pending")
    public String showInventoryManagerApprovalRequisitions(Model model)
    {
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllBySentForPaymentVerification(false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/Inventory_manager/requisitionList";
    }

    @GetMapping("/requisition/inventory_manager/delivery-pending")
    public String showInventoryManagerApprovalRequisitionsForDelivery(Model model)
    {
        System.out.println("hello from delivery pending ");
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllByFirstPaymentApprovedAndApprovedByInventoryManager(true ,false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/Inventory_manager/requisitionList";
    }

    @GetMapping("/requisition/inventory_manager/approveRequisition/{id}")
    public String approveRequisitionByInventoryManager(Model model , @PathVariable("id") long id)
    {
        Requisition requisition = requisitionRepository.getOne(id);
        requisition.setApprovedByInventoryManager(true);
        requisitionRepository.save(requisition);
        System.out.println("hello Approve requisition from delivery manager ");
        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllByFirstPaymentApprovedAndApprovedByInventoryManager(true ,false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/Inventory_manager/requisitionList";
    }





    @GetMapping("/requisition/inventory_manager/sent-for-payment-verification/{id}")
    public String InventoryManagerSentForPaymentVerification(Model model ,@PathVariable("id") long id)
    {

      Requisition requisition = requisitionRepository.getOne(id);
      List<Product> products = new ArrayList<>();
      requisition.getItems().forEach(item -> {
          Product product = productRepository.getOne(item.getProduct_id());
          if (item.getQuantity()>product.getInStock())
          {
             products.add(product);
          }
      });

      if (products.size()<=0)
      {
          requisition.setSentForPaymentVerification(true);
          requisitionRepository.save(requisition);
          model.addAttribute("message" ,"sent for verification successfully");
      }
      else {
          model.addAttribute("shortProducts" , products);
          model.addAttribute("message" , "sent for verification unsuccessfull");
      }

        List<RequisitionDto> requisitionDtos = new ArrayList<>();
        List<Requisition> requisitions = requisitionRepository.findAllBySentForPaymentVerification(false);

        requisitions.forEach(o->{
            RequisitionDto requisitionDto = modelMapper.map(o , RequisitionDto.class);
            requisitionDtos.add(requisitionDto);
        });

        model.addAttribute("requisitions" , requisitionDtos);

        return "/Inventory_manager/requisitionList";
    }

    @GetMapping("/requisition/account_manager/{id}")
    public String approveRequisitionByAccountManager(@PathVariable("id") long id , Model model)
    {
        RequisitionDto requisitionDto = requisitionService.findByid(id);
        requisitionDto.getPayments().forEach(paymentDto -> {
            if (!paymentDto.isReceivedStatus())
            {
                paymentService.approvePayment(paymentDto);
            }
        });
        Requisition requisition = requisitionRepository.getOne(id);
        requisition.setFirstPaymentApproved(true);
        requisitionRepository.save(requisition);

        return "redirect:/approval/requisition/account_manager";
    }
    @GetMapping("/requisition/inventory_manager/{id}")
    public String approveRequisitionByInventoryManager(@PathVariable("id") long id , Model model)
    {
        Requisition requisition = requisitionRepository.getOne(id);
        requisition.setSentForPaymentVerification(true);

        requisitionRepository.save(requisition);

        return "redirect:/approval/requisition/inventory_manager";
    }

}
