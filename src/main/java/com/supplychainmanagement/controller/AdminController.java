package com.supplychainmanagement.controller;

import com.supplychainmanagement.dto.UserDto;
import com.supplychainmanagement.model.User;
import com.supplychainmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/inactive-userList")
    public String approveDealer(Model model)
    {
        List<UserDto> users = userService.findAllInactiveUsers();
        model.addAttribute("users" , users);
        return "/admin/inactive-user-list";
    }

    @GetMapping("/approve-dealer/{id}")

    public String activeUser(@PathVariable("id") long id , RedirectAttributes redirectAttributes){

        userService.enableUserById(id);

        return "redirect:/admin/inactive-userList";

    }
}
