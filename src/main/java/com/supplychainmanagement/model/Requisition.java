package com.supplychainmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Entity(name = "requisition")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Requisition implements Serializable {
    private Random random = new Random();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id ;
    @ManyToOne(fetch = FetchType.EAGER)
     @JoinTable(
             name = "user_requisition" ,
             joinColumns = {@JoinColumn(name = "requisition_id", referencedColumnName = "id")},
             inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")}
     )
    private User user;


    @Column
    private String referenceId ;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(
            name = "requisition_payment" ,
            joinColumns = {@JoinColumn(name = "requisition_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "payment_id", referencedColumnName = "id")}
    )
    private List<Payment> payments;
   @OneToMany(fetch = FetchType.EAGER)
   @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(
            name = "requsition_item" ,
            joinColumns = {@JoinColumn(name = "item_id", referencedColumnName = "id" , updatable = true)},
            inverseJoinColumns = {@JoinColumn(name = "requisition_id", referencedColumnName = "id" ,updatable = true)}
    )
   List<Item> items;

    @CreationTimestamp
    @Column(name = "created_at")
   private LocalDateTime createdAt;
    @Column(name = "total")
    private long total;
    @Column(name = "paymentDue")
    private long paymentDue;
    @Column(name = "sentForPaymentVerification")
    private boolean sentForPaymentVerification = false ;
    @Column(name = "firstPaymentApproved")
    private boolean firstPaymentApproved = false ;
   @Column
   private boolean approvedByInventoryManager = false;

    @Column
    private boolean paymentClear = false;

    @Column
    private boolean receivedByUser = false;
    @Column
    private boolean enabled = true;

    @Column
    private Boolean approvedByDeliveryIncharge = false;

    public void generateReferenceId()
    {
        this.referenceId = RandomString.make(5)+random.nextInt(100);
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(long paymentDue) {
        this.paymentDue = paymentDue;
    }

    public boolean isSentForPaymentVerification() {
        return sentForPaymentVerification;
    }

    public void setSentForPaymentVerification(boolean sentForPaymentVerification) {
        this.sentForPaymentVerification = sentForPaymentVerification;
    }

    public boolean isFirstPaymentApproved() {
        return firstPaymentApproved;
    }

    public void setFirstPaymentApproved(boolean firstPaymentApproved) {
        this.firstPaymentApproved = firstPaymentApproved;
    }

    public boolean isApprovedByInventoryManager() {
        return approvedByInventoryManager;
    }

    public void setApprovedByInventoryManager(boolean approvedByInventoryManager) {
        this.approvedByInventoryManager = approvedByInventoryManager;
    }

    public boolean isPaymentClear() {
        return paymentClear;
    }

    public void setPaymentClear(boolean paymentClear) {
        this.paymentClear = paymentClear;
    }

    public boolean isReceivedByUser() {
        return receivedByUser;
    }

    public void setReceivedByUser(boolean receivedByUser) {
        this.receivedByUser = receivedByUser;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getApprovedByDeliveryIncharge() {
        return approvedByDeliveryIncharge;
    }

    public void setApprovedByDeliveryIncharge(Boolean approvedByDeliveryIncharge) {
        this.approvedByDeliveryIncharge = approvedByDeliveryIncharge;
    }
}
