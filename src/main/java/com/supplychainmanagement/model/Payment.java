package com.supplychainmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Payment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    @Column(name = "paidAmmount")
    long paidAmount;
    @Column(name = "accountNumber")
    private String accountNumber;
    @Column(name = "transactionId")
    private String transactionId;
    @Column(name = "paymentMethod")
    private String paymentMethod;
    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "duePayment")
    private boolean duePayment = false;

    @ManyToOne
    @JoinTable(
            name = "requisition_payment" ,
            joinColumns = {@JoinColumn(name = "payment_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "requisition_id", referencedColumnName = "id")}
    )
   private Requisition requisition;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_payment" ,
            joinColumns = {@JoinColumn(name = "payment_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")}
    )
    private User user;

    @Column(name = "receivedStatus")
    private  boolean receivedStatus= false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(long paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Requisition getRequisition() {
        return requisition;
    }

    public void setRequisition(Requisition requisition) {
        this.requisition = requisition;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isReceivedStatus() {
        return receivedStatus;
    }

    public void setReceivedStatus(boolean receivedStatus) {
        this.receivedStatus = receivedStatus;
    }

    public boolean isDuePayment() {
        return duePayment;
    }

    public void setDuePayment(boolean duePayment) {
        this.duePayment = duePayment;
    }
}
