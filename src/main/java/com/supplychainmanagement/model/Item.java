package com.supplychainmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   private long id;
    @Column(name = "name")
    private String name ;
    @Column(name = "price")
   private long price;
    @Column(name = "quantity")
   private int quantity;
    @Column(name = "quantity_per_carton")
    int quantity_per_carton;
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "item_image",
            joinColumns = {@JoinColumn(name = "item_id", referencedColumnName = "id" ,updatable = true)},
            inverseJoinColumns = {@JoinColumn(name = "image_id", referencedColumnName = "id" , updatable = true)}


    )
    private List<Image> imageList;
//    @ManyToOne(fetch = FetchType.EAGER , cascade = CascadeType.MERGE)
//    @JoinTable(
//            name = "requsition_item" ,
//            joinColumns = {@JoinColumn(name = "item_id", referencedColumnName = "id" ,updatable = true )},
//            inverseJoinColumns = {@JoinColumn(name = "requisition_id", referencedColumnName = "id" ,updatable = true)}
//    )
//    Requisition requisition ;

    @Column(name = "product_id")
    private long product_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity_per_carton() {
        return quantity_per_carton;
    }

    public void setQuantity_per_carton(int quantity_per_carton) {
        this.quantity_per_carton = quantity_per_carton;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }
}
